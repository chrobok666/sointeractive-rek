package igor.sointeractive.app.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import igor.sointeractive.app.model.Profile;

@RestController
public class HelloController {


    
    @RequestMapping("/")
    public String index() {
    	
    	
        return "Greetings from Spring Boot!";
    }
    
    @RequestMapping(value="/mapping", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String byConsumes(@RequestBody Profile profile) {
        return "Mapped by path + method + consumable media type (javaBean '" + profile.getBirthday() + "')";
    }

}


